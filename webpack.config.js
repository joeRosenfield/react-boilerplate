var path = require('path');

var config = {

   entry: './src/app.js',

   output: {
      path: "./builds",
      filename: './src/app.js',
   },

   devServer: {
      inline: true,
      port: 8080,
      // hot: true
   },

   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',

            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}

module.exports = config;
