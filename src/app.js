import React from 'react';
import ReactDOM from 'react-dom';
import DefaultScreen from './components/screens/DefaultScreen';

ReactDOM.render(<DefaultScreen />, document.getElementById('app'));
