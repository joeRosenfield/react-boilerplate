import React from 'react';
import Headers from '../elements/Headers.js';
import Body from '../elements/Body.js';
import Footers from '../elements/Footers.js';
import FieldContainer from '../organisms/FieldContainer'
import FieldTextInput from '../organisms/FieldTextInput'
import FieldPasswordInput from '../organisms/FieldPasswordInput'
import FieldConfirmPasswordInput from '../organisms/FieldConfirmPasswordInput'

class DefaultScreen extends React.Component {

    constructor(props) {

      super(props);

      this.state = {
        username: '',
        password: '',
        confirmPassword: ''
      }

    }

    handleUsernameChange (newValue) {
      this.setState({
        username: newValue
      })
    }

    handlePasswordChange (newValue) {
      this.setState({
        password: newValue
      })
    }

    handleConfirmPasswordChange (newValue) {
      this.setState({
        confirmPassword: newValue
      });
    }


    handleFormSubmit () {
      console.log('Form Submits, DefaultScreen state:', this.state);
    }


   render() {

     console.debug(this.state);


      return (
        <div>

            <Headers>
              <h1 style={styles.title}>Registration Form</h1>
            </Headers>

            <Body>

            <FieldContainer title="Username:">
              <FieldTextInput onChange={ (value) => this.handleUsernameChange(value)} />
            </FieldContainer>


            <FieldContainer title="Password:">
              <FieldPasswordInput onChange={ (value) => this.handlePasswordChange(value)} />
            </FieldContainer>


            <FieldContainer title="Confirm Password:">
              <FieldConfirmPasswordInput onChange={ (value) => this.handleConfirmPasswordChange(value)} />
            </FieldContainer>




            </Body>

            <Footers>
              <button style={styles.button} onClick={ () => this.handleFormSubmit() }>Submit</button>
              <button style={styles.backButton} onClick={ () => this.handleFormSubmit() }>Cancel</button>
            </Footers>

         </div>
      );
   }
}

const styles = {

  title: {
    margin: '3%',
    marginTop: '4%',
    color: 'white',
    fontSize: '4em'
  },

  input: {
    margin: '1%',
    padding: '1%',
    borderStyle: 'none',
    backgroundColor: 'rgba(238, 238, 238, 0.8)'
  },

  fieldContainer: {
    width: '92%',
    height: 'auto',
    margin: '2% 2% 3% 3%',
    padding: '1%'
  },

  fieldTitle: {
    marginLeft: '1%',
    fontFamily: 'Arial'
  },


  button: {
    position: 'absolute',
    width: '40%',
    height: '50%',
    right: '5%',
    top: '24%',
    padding: '2%',
    borderStyle: 'none',
    backgroundColor: 'lightBlue',
    color: 'white',
    fontSize: '3em',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },

  backButton: {
    position: 'absolute',
    width: '40%',
    height: '50%',
    left: '5%',
    top: '24%',
    padding: '2%',
    borderStyle: 'none',
    backgroundColor: 'red',
    color: 'white',
    fontSize: '3em',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  }


}

export default DefaultScreen;
