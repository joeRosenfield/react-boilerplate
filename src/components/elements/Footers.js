import React from 'react';


class Footers extends React.Component {
   render() {
      return (
        <div className="footers" style={styles.div}>
          {this.props.children}
        </div>
      );
   }
}

const styles = {
  div: {
    position: 'fixed',
    zIndex: '1',
    width: '100%',
    height: '15%',
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(238, 238, 238, 0.4)'
  }
}

export default Footers;
