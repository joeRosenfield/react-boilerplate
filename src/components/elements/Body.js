import React from 'react';


class Body extends React.Component {
   render() {
      return (
        <div className="body" style={styles.div}>
          {this.props.children}
        </div>
      );
   }
}

const styles = {
  div: {
    position: 'absolute',
    width: '100%',
    height: '85%',
    top: '10%',
    left: 0,
    overflowY : 'scroll'
    
  }
}

export default Body;
