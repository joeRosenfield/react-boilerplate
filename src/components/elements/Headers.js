import React from 'react';


class Headers extends React.Component {
   render() {
      return (
        <div className="headers" style={styles.div}>
          {this.props.children}
        </div>
      );
   }
}

const styles = {
  div: {
    position: 'fixed',
    zIndex: '1',
    width: '100%',
    height: '10%',
    top: 0,
    left: 0,
    textAlign: 'left',
    fontFamily: 'Arial',
    backgroundColor: 'lightBlue'
  }
}

export default Headers;
