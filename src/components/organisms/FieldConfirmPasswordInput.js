import React from 'react';

class FieldConfirmPasswordInput extends React.Component {

    propTypes: {
      style: React.PropTypes.object,
      onChange: React.PropTypes.func
    }

    constructor (props) {

      super(props);

      this.state = {
        showPlaceholder: true,
        placeholder: 'Confirm Your Password...'
      }

    }

    onChange () {

      var value = this.refs.password.value;

      if(this.props.onChange) {
        this.props.onChange( value );
      }

    }

    render() {
      //
      // var inputStyles = [styles.input];
      //
      // if(this.prop.style) {
      //   inputStyles.push( this.props.style );
      // }

      return (this.state.showPlaceholder) ? this.renderPlaceholder() : this.renderProtectedField();

    }

    renderPlaceholder () {

      var placeholderText = this.props.placeholder ? this.props.placeholder : this.state.placeholder

      return (
        <input
          style={ styles.input }
          type="text"
          onClick={ () => this.setState({showPlaceholder: false}) }
          placeholder={placeholderText}/>
      )

    }

    renderProtectedField () {
      return (
        <input ref="password" style={ styles.input } type="password" onChange={ () => this.onChange() } />
      )
    }

}

const styles = {

  input: {
    width: '90%',
    margin: '5% 0 0 1%',
    padding: '5%',
    borderStyle: 'none',
    backgroundColor: 'rgba(238, 238, 238, 0.8)'
  }

}

export default FieldConfirmPasswordInput;
