import React from 'react';

class FieldTextInput extends React.Component {

    propTypes: {
      style: React.PropTypes.object,
      onChange: React.PropTypes.func
    }

    onChange () {

      var value = this.refs.input.value;

      if(this.props.onChange) {
        this.props.onChange( value );
      }

    }

    render() {
      //
      // var inputStyles = [styles.input];
      //
      // if(this.prop.style) {
      //   inputStyles.push( this.props.style );
      // }

      /* First off, this is how you comment in jsx... This is where the state handler story starts */
      return (
        <input style={ styles.input }
         type="text" ref="input"
         placeholder="Pick A Username..."
         onChange={ () => this.onChange() } />
      )

    }

}

const styles = {

  input: {
    width: '90%',
    margin: '5% 0 0 1%',
    padding: '5%',
    borderStyle: 'none',
    backgroundColor: 'rgba(238, 238, 238, 0.8)'
  }

}

export default FieldTextInput;
