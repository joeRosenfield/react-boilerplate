import React from 'react';

class FieldContainer extends React.Component {

    propTypes: {
      title: React.PropTypes.string
    }

    render() {
      return (
        <div style={styles.fieldContainer}>
          <p style={styles.fieldTitle}>{this.props.title}</p>
          {this.props.children}
        </div>
      );
    }

}

const styles = {

  fieldContainer: {
    width: '92%',
    height: 'auto',
    margin: '2% 2% 3% 3%',
    padding: '1%'
  },

  fieldTitle: {
    marginLeft: '1%',
    fontFamily: 'Arial',
    fontSize: '1.5em'
  }

}

export default FieldContainer;
